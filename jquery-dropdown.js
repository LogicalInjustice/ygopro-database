var json = {
    "Destinations": [
	{
		"id": "6130",
		"name": "Performapal Handsome Liger"
	},
	{
		"id":"6131",
		"name":"Fluffal Patchwork"
	}
    ]
};

var cards = {
    "Types": [
    {
        "hex": "0x1",
        "val": 'Monster'
    }, {
        "hex": "0x2",
        "val": 'Spell'
    }, {
        "hex": "0x4",
        "val": 'Trap'
    }, {
        "hex": "0x10",
        "val": 'Normal'
    }, {
        "hex": "0x20",
        "val": 'Effect'
    }, {
        "hex": "0x40",
        "val": 'Fusion'
    }, {
        "hex": "0x80",
        "val": 'Ritual'
    }, {
        "hex": "0x100",
        "val": 'TrapMonster'
    }, {
        "hex": "0x200",
        "val": 'Spirit'
    }, {
        "hex": "0x400",
        "val": 'Union'
    }, {
        "hex": "0x800",
        "val": 'Gemini'
    }, {
        "hex": "0x1000",
        "val": 'Tuner'
    }, {
        "hex": "0x2000",
        "val": 'Synchro'
    }, {
        "hex": "0x4000",
        "val": 'Token'
    }, {
        "hex": "0x10000",
        "val": 'Quick-Play'
    }, {
        "hex": "0x20000",
        "val": 'Continuous'
    }, {
        "hex": "0x40000",
        "val": 'Equip'
    }, {
        "hex": "0x80000",
        "val": 'Field'
    }, {
        "hex": "0x100000",
        "val": 'Counter'
    }, {
        "hex": "0x200000",
        "val": 'Flip'
    }, {
        "hex": "0x400000",
        "val": 'Toon'
    }, {
        "hex": "0x800000",
        "val": 'Xyz'
    }, {
        "hex": "0x1000000",
        "val": 'Pendulum'
    }
    ]
};

$('#fetch').click(function() {
    $.post('/echo/json/', {json: JSON.stringify(json)}, function(data) {
        $.each(data.Destinations, function(i, v) {
            $('#ot').append('<option value="' + v.id + '">' + v.name + '</option>');
        });
    });
    $.post('/echo/json/', {json: JSON.stringify(cards)}, function(data2) {
        $.each(data2.Types, function(i, v) {
            $('#type').append('<option value="' + v.hex + '">' + v.val + '</option>');
        });
    });
});
